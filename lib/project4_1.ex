defmodule Project4_1 do
  @moduledoc """
  Documentation for Project41.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Project41.hello()
      :world

  """
  def hello do
    :world
  end
end
