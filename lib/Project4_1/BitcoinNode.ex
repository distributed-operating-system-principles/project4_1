defmodule Project4_1.BitcoinNode do
  use GenServer
  @reward 50.0
  @difficulty 8
  @easiness 256 - @difficulty
  @num_txns_block 3
  @mining_timer 2000

  def start_link(main_pid, opts \\ []) do
    GenServer.start_link(__MODULE__, main_pid, opts)
  end

  def set_list_pids(node, list_pids) do
    GenServer.call(node, {:list_pids, list_pids})
  end

  def send_money(node, receiver_address, amount) do
    GenServer.call(node, {:send_money, receiver_address, amount})
  end

  def init(main_pid) do
    state = %{}
    state = Map.put(state, :txn_utxos, %{})
    state = Map.put(state, :wallet, Project4_1.Wallet.new())
    state = Map.put(state, :block_map, %{})
    state = Map.put(state, :last_block_map, %{})
    state = Map.put(state, :pending_transactions, %{})
    state = Map.put(state, :list_pids, [])
    state = Map.put(state, :main_pid, main_pid)

    Process.send_after(self(), :mine, @mining_timer)

    {:ok, state}
  end

  def handle_call({:send_money, receiver_address, amount}, _from, state) do
    wallet = Map.get(state, :wallet)

    {status, new_transaction} =
      case Project4_1.Wallet.create_new_transaction(wallet, receiver_address, amount) do
        :nofunds -> {:nofunds, nil}
        {:ok, new_transaction} -> {:ok, new_transaction}
      end

    # IO.inspect(new_transaction)

    if(status === :ok) do
      broadcast_transaction(new_transaction, state)
    end

    {:reply, status, state}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:get_wallet_address, _from, state) do
    {:reply, Project4_1.Wallet.get_wallet_address(Map.get(state, :wallet)), state}
  end

  def handle_call({:list_pids, list_pids}, _from, state) do
    state = Map.put(state, :list_pids, list_pids)
    GenServer.cast(self(), :create_genesis_block)
    {:reply, :ok, state}
  end

  def handle_cast(:create_genesis_block, state) do
    {:ok, coinbase_txn} =
      Project4_1.Wallet.create_coinbase_transaction(Map.get(state, :wallet), @reward)

    transaction_map = %{Project4_1.Utils.hash_transaction(coinbase_txn) => coinbase_txn}

    hashMerkleRoot = get_merkle_tree_root(transaction_map, Enum.count(transaction_map))

    # IO.puts("Merkle root- #{Kernel.inspect(self())}: #{Kernel.inspect(hashMerkleRoot)}")

    genesis_block_header = Project4_1.BlockHeader.new(nil, hashMerkleRoot, @difficulty, 0)

    genesis_block = Project4_1.Block.new(genesis_block_header, transaction_map)

    {status, mined_genesis_block} = mine_block(genesis_block)

    state =
      if(status === :broadcast) do
        broadcast_block(mined_genesis_block, state)
        state
      else
        add_block_to_chain(mined_genesis_block, state)
      end

    {:noreply, state}
  end

  def handle_cast({:receive_transaction, transaction}, state) do
    pending_transactions = Map.get(state, :pending_transactions)

    pending_transactions =
      Map.put(pending_transactions, Project4_1.Utils.hash_transaction(transaction), transaction)

    state =
      if(Project4_1.Transaction.legitimate?(transaction, Map.get(state, :txn_utxos))) do
        txn = transaction[:txn]
        outputs = txn[:outputs]
        inputs = txn[:inputs]
        wallet = state[:wallet]
        txn_utxos = state[:txn_utxos]

        {txn_utxos, wallet} =
          Enum.reduce(inputs, {txn_utxos, wallet}, fn input, {txn_utxos, wallet} ->
            txn_utxos = Map.delete(txn_utxos, {input[:hash], input[:output_index]})

            wallet =
              if(Project4_1.Wallet.get_public_key(wallet) === input[:public_key]) do
                Project4_1.Wallet.remove_utxo(wallet, {input[:hash], input[:output_index]})
              else
                wallet
              end

            {txn_utxos, wallet}
          end)

        utxo0 = %{
          confirmation_number: 0,
          amount: Enum.at(outputs, 0)[:amount],
          transaction_string: Kernel.inspect(txn),
          address: Enum.at(outputs, 0)[:receiver_address]
        }

        utxo1 = %{
          confirmation_number: 0,
          amount: Enum.at(outputs, 1)[:amount],
          transaction_string: Kernel.inspect(txn),
          address: Enum.at(outputs, 1)[:receiver_address]
        }

        wallet =
          if(utxo0[:address] === Project4_1.Wallet.get_wallet_address(wallet)) do
            Process.send_after(Map.get(state, :main_pid), {:genesis_block, self()}, 0)

            Project4_1.Wallet.put_utxo(
              wallet,
              {Project4_1.Utils.hash_transaction(transaction), 0},
              Kernel.inspect(txn),
              0,
              utxo0[:amount],
              Project4_1.Wallet.sign_transaction(wallet, transaction),
              Project4_1.Wallet.get_public_key(wallet)
            )
          else
            wallet
          end

        wallet =
          if(utxo1[:address] === Project4_1.Wallet.get_wallet_address(wallet)) do
            Project4_1.Wallet.put_utxo(
              wallet,
              {Project4_1.Utils.hash_transaction(transaction), 1},
              Kernel.inspect(txn),
              0,
              utxo1[:amount],
              Project4_1.Wallet.sign_transaction(wallet, transaction),
              Project4_1.Wallet.get_public_key(wallet)
            )
          else
            wallet
          end

        txn_utxos = Map.put(txn_utxos, {Project4_1.Utils.hash_transaction(transaction), 0}, utxo0)
        txn_utxos = Map.put(txn_utxos, {Project4_1.Utils.hash_transaction(transaction), 1}, utxo1)

        state = Map.put(state, :txn_utxos, txn_utxos)
        state = Map.put(state, :wallet, wallet)

        Map.put(state, :pending_transactions, pending_transactions)
      else
        state
      end

    # IO.puts(
    #   "#{Kernel.inspect(self())}: #{Project4_1.Wallet.get_wallet_balance(Map.get(state, :wallet))}"
    # )

    {:noreply, state}
  end

  def handle_info(:mine, state) do
    pending_transactions = Map.get(state, :pending_transactions)

    state =
      if(Enum.count(pending_transactions) >= @num_txns_block) do
        {:ok, coinbase_txn} =
          Project4_1.Wallet.create_coinbase_transaction(Map.get(state, :wallet), @reward)

        pending_transactions =
          Map.put(
            pending_transactions,
            Project4_1.Utils.hash_transaction(coinbase_txn),
            coinbase_txn
          )

        hashMerkleRoot =
          get_merkle_tree_root(pending_transactions, Enum.count(pending_transactions))

        block_header =
          Project4_1.BlockHeader.new(
            Project4_1.Utils.hash_block_header(get_last_block(state)),
            hashMerkleRoot,
            @difficulty,
            0
          )

        genesis_block = Project4_1.Block.new(block_header, pending_transactions)

        {status, mined_genesis_block} = mine_block(genesis_block)

        if(status === :broadcast) do
          broadcast_block(mined_genesis_block, state)
          state
        else
          add_block_to_chain(mined_genesis_block, state)
        end
      else
        state
      end

    # IO.puts("#{Kernel.inspect(self())}: mining")

    Process.send_after(self(), :mine, @mining_timer)
    {:noreply, state}
  end

  def handle_info({:receive_block, recvd_block}, state) do
    # IO.puts("#{Kernel.inspect(self())}: #{Kernel.inspect(recvd_block)}")
    state = add_block_to_chain(recvd_block, state)

    {:noreply, state}
  end

  def broadcast_transaction(transaction, state) do
    list_pids = Map.get(state, :list_pids)

    Enum.reduce(list_pids, :ok, fn pid, _res ->
      GenServer.cast(pid, {:receive_transaction, transaction})
      :ok
    end)
  end

  def get_last_block(state) do
    last_block_map = Map.get(state, :last_block_map)

    {fin_block, _fin_height} =
      Enum.reduce(last_block_map, {nil, -1}, fn {_, {block, height}}, {fin_block, fin_height} ->
        if(height > fin_height) do
          {block, height}
        else
          {fin_block, fin_height}
        end
      end)

    # IO.puts("Last Block Map: #{Kernel.inspect(last_block_map)}")

    fin_block
  end

  def add_block_to_chain(block, state) do
    transactions = block[:transactions]

    # IO.puts("Block: #{Kernel.inspect(block)}")

    state =
      Enum.reduce(transactions, state, fn {_, transaction}, state ->
        txn = transaction[:txn]
        outputs = txn[:outputs]
        inputs = txn[:inputs]
        pending_transactions = state[:pending_transactions]

        state =
          if(inputs === []) do
            wallet = state[:wallet]
            txn_utxos = state[:txn_utxos]

            utxo0 = %{
              confirmation_number: 0,
              amount: Enum.at(outputs, 0)[:amount],
              transaction_string: Kernel.inspect(txn),
              address: Enum.at(outputs, 0)[:receiver_address]
            }

            utxo1 = %{
              confirmation_number: 0,
              amount: Enum.at(outputs, 1)[:amount],
              transaction_string: Kernel.inspect(txn),
              address: Enum.at(outputs, 1)[:receiver_address]
            }

            wallet =
              if(utxo0[:address] === Project4_1.Wallet.get_wallet_address(wallet)) do
                Process.send_after(Map.get(state, :main_pid), {:genesis_block, self()}, 0)

                Project4_1.Wallet.put_utxo(
                  wallet,
                  {Project4_1.Utils.hash_transaction(transaction), 0},
                  Kernel.inspect(txn),
                  0,
                  utxo0[:amount],
                  Project4_1.Wallet.sign_transaction(wallet, transaction),
                  Project4_1.Wallet.get_public_key(wallet)
                )
              else
                wallet
              end

            wallet =
              if(utxo1[:address] === Project4_1.Wallet.get_wallet_address(wallet)) do
                Project4_1.Wallet.put_utxo(
                  wallet,
                  {Project4_1.Utils.hash_transaction(transaction), 1},
                  Kernel.inspect(txn),
                  0,
                  utxo1[:amount],
                  Project4_1.Wallet.sign_transaction(wallet, transaction),
                  Project4_1.Wallet.get_public_key(wallet)
                )
              else
                wallet
              end

            txn_utxos =
              Map.put(txn_utxos, {Project4_1.Utils.hash_transaction(transaction), 0}, utxo0)

            txn_utxos =
              Map.put(txn_utxos, {Project4_1.Utils.hash_transaction(transaction), 1}, utxo1)

            state = Map.put(state, :txn_utxos, txn_utxos)
            Map.put(state, :wallet, wallet)
          else
            state
          end

        pending_transactions =
          Map.delete(pending_transactions, Project4_1.Utils.hash_transaction(transaction))

        state = Map.put(state, :pending_transactions, pending_transactions)
        state
      end)

    block_header = block[:header]
    prev_block_hash = block_header[:hash_prev_block]
    block_map = Map.get(state, :block_map)
    last_block_map = Map.get(state, :last_block_map)

    {block_map, last_block_map} =
      cond do
        prev_block_hash === nil ->
          {block_map,
           Map.put(last_block_map, Project4_1.Utils.hash_block_header(block), {block, 0})}

        last_block_map[prev_block_hash] !== nil ->
          temp = last_block_map[prev_block_hash]
          last_block_map = Map.delete(last_block_map, prev_block_hash)

          {Map.put(block_map, prev_block_hash, temp),
           Map.put(
             last_block_map,
             Project4_1.Utils.hash_block_header(block),
             {block, elem(temp, 1) + 1}
           )}

        true ->
          {block_map,
           Map.put(
             last_block_map,
             Project4_1.Utils.hash_block_header(block),
             {block, elem(block_map[prev_block_hash], 1) + 1}
           )}
      end

    state = Map.put(state, :last_block_map, last_block_map)

    Map.put(state, :block_map, block_map)
  end

  def broadcast_block(block, state) do
    list_pids = Map.get(state, :list_pids)

    Enum.reduce(list_pids, :ok, fn pid, _res ->
      Process.send_after(pid, {:receive_block, block}, 0)
      :ok
    end)
  end

  def mine_block(block) do
    block = %{block | header: %{block.header | nonce: block.header.nonce + 1}}

    if(check_block_mined(block) === true) do
      # IO.puts("Mined- #{Kernel.inspect(self())}")
      {:broadcast, block}
    else
      ret =
        receive do
          {:receive_block, recvd_block} ->
            {:nobroadcast, recvd_block}
        after
          1 -> mine_block(block)
        end

      ret
    end
  end

  def check_block_mined(block) do
    hashed_block = Project4_1.Utils.hash_block_header(block)
    <<init_bits::@difficulty, _rest_bits::@easiness>> = hashed_block

    if(init_bits === 0) do
      true
    else
      false
    end
  end

  # def get_merkle_tree_root(transaction_map, 1) do
  #   elem(Enum.at(transaction_map, 0), 1)
  # end

  def get_merkle_tree_root(transaction_map, map_length) do
    {result, _index} =
      Enum.reduce(transaction_map, {%{}, 0}, fn {key, transaction}, {res, idx} ->
        res =
          cond do
            rem(idx, 2) === 0 && idx < map_length - 1 ->
              Map.put(
                res,
                key,
                Project4_1.Keys.get_hash(
                  Kernel.inspect(transaction) <>
                    Kernel.inspect(elem(Enum.at(transaction_map, idx + 1), 1))
                )
              )

            rem(idx, 2) === 0 && idx === map_length - 1 ->
              Map.put(
                res,
                key,
                Project4_1.Keys.get_hash(
                  Kernel.inspect(transaction) <> Kernel.inspect(transaction)
                )
              )

            true ->
              res
          end

        {res, idx + 1}
      end)

    if(Enum.count(result) === 1) do
      elem(Enum.at(result, 0), 1)
    else
      get_merkle_tree_root(result, Enum.count(result))
    end
  end

  # def get_merkle_tree_root(transaction_map, map_length) do
  #   transaction_list =
  #     if(is_map(transaction_map)) do
  #       convert_map_to_list_for_merkle_tree(transaction_map)
  #     else
  #       transaction_map
  #     end

  #   {result, _index} =
  #     Enum.reduce(transaction_list, {[], 0}, fn transaction, {res, idx} ->
  #       res =
  #         cond do
  #           rem(idx, 2) === 0 && idx < map_length - 1 ->
  #             [|res]
  #             Map.put(
  #               res,
  #               key,
  #               Project4_1.Keys.get_hash(
  #                 Kernel.inspect(transaction) <> Kernel.inspect(Enum.at(transaction_map, idx + 1))
  #               )
  #             )

  #           rem(idx, 2) === 0 && idx === map_length - 1 ->
  #             Map.put(
  #               res,
  #               key,
  #               Project4_1.Keys.get_hash(
  #                 Kernel.inspect(transaction) <> Kernel.inspect(transaction)
  #               )
  #             )

  #           true ->
  #             res
  #         end

  #       {res, idx + 1}
  #     end)

  #   if(Enum.count(result) === 1) do
  #     elem(Enum.at(result, 0), 1)
  #   else
  #     get_merkle_tree_root(result, Enum.count(result))
  #   end
  # end

  # def convert_map_to_list_for_merkle_tree(transaction_map) do
  #   reverse_list(
  #     Enum.reduce(transaction_map, [], fn {_, transaction}, res_list ->
  #       [Project4_1.Keys.get_hash(Kernel.inspect(transaction)) | res_list]
  #     end)
  #   )
  # end

  # def reverse_list(res_list) do
  #   for i <- (Enum.count(res_list) - 1)..0 do
  #     Enum.at(res_list, i)
  #   end
  # end
end
