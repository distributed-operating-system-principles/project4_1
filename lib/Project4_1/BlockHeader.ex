defmodule Project4_1.BlockHeader do
  def new(block_header \\ %{}) do
    block_header = Map.put(block_header, :hash_prev_block, nil)
    block_header = Map.put(block_header, :hash_merkle_root, nil)
    block_header = Map.put(block_header, :difficulty, 0)
    block_header = Map.put(block_header, :nonce, 0)
    block_header
  end

  def new(block_header \\ %{}, hashPrevBlock, hashMerkleRoot, difficulty, nonce) do
    block_header = Map.put(block_header, :hash_prev_block, hashPrevBlock)
    block_header = Map.put(block_header, :hash_merkle_root, hashMerkleRoot)
    block_header = Map.put(block_header, :difficulty, difficulty)
    block_header = Map.put(block_header, :nonce, nonce)
    block_header
  end
end
