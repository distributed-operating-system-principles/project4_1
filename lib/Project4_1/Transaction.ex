defmodule Project4_1.Transaction do
  # %{txn: %{inputs: , outputs: }, signature: {signature, public_key}}
  # input: %{hash: , output_index: , signature: , public_key: , amount: }
  # output: %{amount: , receiver_address: }

  def new(transaction \\ %{}, txn \\ %{}) do
    txn = Map.put(txn, :inputs, [])
    txn = Map.put(txn, :outputs, [])
    transaction = Map.put(transaction, :txn, txn)
    transaction = Map.put(transaction, :signature, {})
    transaction
  end

  def new(
        self_address,
        input_transactions,
        input_amount,
        receiver_address,
        output_amount,
        {private_key, public_key}
      ) do
    outputs = [
      %{amount: output_amount, receiver_address: receiver_address},
      %{amount: input_amount - output_amount, receiver_address: self_address}
    ]

    transaction = Project4_1.Transaction.new()
    txn = %{inputs: input_transactions, outputs: outputs}
    transaction = Map.put(transaction, :txn, txn)

    Map.put(
      transaction,
      :signature,
      {Project4_1.Utils.sign_transaction(transaction, private_key), public_key}
    )
  end

  # transactions means utxos
  def legitimate?(transaction, transactions) do
    inputs = transaction[:txn][:inputs]
    {signature, public_key} = transaction[:signature]

    verify_inputs(inputs, transactions) && verify_outputs(transaction, signature, public_key)
  end

  def verify_outputs(transaction, signature, public_key) do
    Project4_1.Utils.verify_signed_transaction(transaction, signature, public_key)
  end

  def verify_inputs([], _transactions) do
    true
  end

  def verify_inputs(inputs, transactions) do
    [input | inputs] = inputs
    transaction_hash = input[:hash]
    output_index = input[:output_index]
    signature = input[:signature]
    public_key = input[:public_key]
    referred_transaction = Map.get(transactions, {transaction_hash, output_index})
    # referred_outputs = referred_transaction[:txn][:outputs]
    # address = Enum.at(referred_outputs, output_index)[:receiver_address]
    address = referred_transaction[:address]

    if(
      address === Project4_1.Keys.get_address(public_key) &&
        Project4_1.Utils.verify_signed_transaction_string(
          referred_transaction[:transaction_string],
          signature,
          public_key
        ) === true
    ) do
      verify_inputs(inputs, transactions)
    else
      false
    end
  end
end
