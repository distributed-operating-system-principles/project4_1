defmodule Project4_1.Wallet do
  def new(wallet \\ %{}) do
    {:ok, private_key, public_key} = Project4_1.Keys.create_pair()
    wallet = Map.put(wallet, :balance, 0.0)
    wallet = Map.put(wallet, :public_key, public_key)
    wallet = Map.put(wallet, :private_key, private_key)
    wallet = Map.put(wallet, :address, Project4_1.Keys.get_address(public_key))
    wallet = Map.put(wallet, :utxos, %{})

    wallet
  end

  def get_wallet_address(wallet) do
    Map.get(wallet, :address)
  end

  def get_wallet_balance(wallet) do
    Map.get(wallet, :balance)
  end

  def get_public_key(wallet) do
    Map.get(wallet, :public_key)
  end

  def put_utxo(
        wallet,
        {transaction_hash, output_index},
        transaction_string,
        confirmation_number,
        amount,
        signature,
        public_key
      ) do
    utxos = Map.get(wallet, :utxos)

    utxo = %{}
    utxo = Map.put(utxo, :confirmation_number, confirmation_number)
    utxo = Map.put(utxo, :signature, signature)
    utxo = Map.put(utxo, :public_key, public_key)
    utxo = Map.put(utxo, :amount, amount)
    utxo = Map.put(utxo, :transaction_string, transaction_string)
    utxo = Map.put(utxo, :address, get_wallet_address(wallet))

    utxos = Map.put(utxos, {transaction_hash, output_index}, utxo)

    wallet = add_money(wallet, amount)
    Map.put(wallet, :utxos, utxos)
  end

  def remove_utxo(wallet, {transaction_hash, output_index}) do
    utxos = Map.get(wallet, :utxos)
    amount = Map.get(utxos, {transaction_hash, output_index})[:amount]
    utxos = Map.delete(utxos, {transaction_hash, output_index})
    {_status, wallet} = remove_money(wallet, amount)
    Map.put(wallet, :utxos, utxos)
  end

  def sign_transaction(wallet, transaction) do
    private_key = Map.get(wallet, :private_key)
    Project4_1.Utils.sign_transaction(transaction, private_key)
  end

  def sign_transaction_string(wallet, transaction_string) do
    private_key = Map.get(wallet, :private_key)
    Project4_1.Utils.sign_transaction_string(transaction_string, private_key)
  end

  def create_new_transaction(wallet, receiver_address, amount) do
    case get_input_utxos(wallet, amount) do
      {:nofunds} ->
        :nofunds

      {:ok, input_amount, input_transactions} ->
        {:ok,
         Project4_1.Transaction.new(
           Map.get(wallet, :address),
           input_transactions,
           input_amount,
           receiver_address,
           amount,
           {Map.get(wallet, :private_key), Map.get(wallet, :public_key)}
         )}
    end
  end

  def create_coinbase_transaction(wallet, amount) do
    {:ok,
     Project4_1.Transaction.new(
       Map.get(wallet, :address),
       [],
       amount,
       Map.get(wallet, :address),
       amount,
       {Map.get(wallet, :private_key), Map.get(wallet, :public_key)}
     )}
  end

  def publish_new_transaction(wallet, transaction) do
    inputs = transaction[:txn][:inputs]

    Enum.reduce(inputs, wallet, fn input, wallet ->
      remove_utxo(wallet, {input[:hash], input[:output_index]})
    end)
  end

  def get_input_utxos(wallet, output_amount) do
    utxos = Map.get(wallet, :utxos)

    {transaction_sum, result} =
      Enum.reduce(utxos, {0, []}, fn {{transaction_hash, output_index}, utxo}, {acc, res} ->
        res =
          if(acc <= output_amount) do
            [
              %{
                hash: transaction_hash,
                output_index: output_index,
                signature: utxo[:signature],
                public_key: utxo[:public_key],
                amount: utxo[:amount]
              }
              | res
            ]
          else
            res
          end

        acc = acc + utxo[:amount]

        {acc, res}
      end)

    if(transaction_sum < output_amount) do
      {:nofunds}
    else
      {:ok, transaction_sum, result}
    end
  end

  def add_money(wallet, amount) do
    wallet = Map.put(wallet, :balance, Map.get(wallet, :balance) + amount)
    wallet
  end

  def remove_money(wallet, amount) do
    res =
      if(amount <= Map.get(wallet, :balance)) do
        {:ok, Map.put(wallet, :balance, Map.get(wallet, :balance) - amount)}
      else
        {:nofunds, wallet}
      end

    res
  end
end
