defmodule Project4_1.Utils do
  def convert_map_to_list(m) when not is_map(m) do
    m
  end

  def convert_map_to_list(m) when is_map(m) do
    m = Map.from_struct(m)
    m |> Enum.map(fn {key, value} -> [to_string(key), to_string(convert_map_to_list(value))] end)
  end

  def rand_pid(pid, list_pids, num_nodes) do
    temp = Enum.at(list_pids, :rand.uniform(num_nodes) - 1)

    if(temp !== pid) do
      temp
    else
      rand_pid(pid, list_pids, num_nodes)
    end
  end

  def hash_transaction(transaction) do
    Project4_1.Keys.get_hash(Kernel.inspect(transaction[:txn]))
  end

  def sign_transaction(transaction, private_key) do
    Project4_1.Keys.sign(Kernel.inspect(transaction[:txn]), private_key)
  end

  def verify_signed_transaction(transaction, signature, public_key) do
    Project4_1.Keys.verify(Kernel.inspect(transaction[:txn]), signature, public_key)
  end

  def sign_transaction_string(transaction_string, private_key) do
    Project4_1.Keys.sign(transaction_string, private_key)
  end

  def verify_signed_transaction_string(transaction_string, signature, public_key) do
    Project4_1.Keys.verify(transaction_string, signature, public_key)
  end

  def hash_block_header(block) do
    Project4_1.Keys.get_hash(Kernel.inspect(block[:header]))
  end
end
