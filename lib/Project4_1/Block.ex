defmodule Project4_1.Block do
  def new(block \\ %{}) do
    block = Map.put(block, :header, nil)
    block = Map.put(block, :transactions, nil)
    block
  end

  def new(block \\ %{}, header, transactions) do
    block = Map.put(block, :header, header)
    block = Map.put(block, :transactions, transactions)
    block
  end
end
