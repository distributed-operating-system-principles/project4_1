defmodule Project41Test do
  use ExUnit.Case
  # doctest Project4_1

  def start_genservers(num_nodes) do
    for _i <- 1..num_nodes do
      {:ok, pid} = Project4_1.BitcoinNode.start_link(self())
      {pid, GenServer.call(pid, :get_state)}
    end
  end

  def kill_genservers(list_pids) do
    for {pid, _state} <- list_pids do
      Process.exit(pid, :kill)
    end
  end

  def rand_pid(pid, list_pids, num_nodes) do
    temp = Enum.at(list_pids, :rand.uniform(num_nodes) - 1)

    if(temp !== pid) do
      temp
    else
      rand_pid(pid, list_pids, num_nodes)
    end
  end

  def create_transaction() do
    wallet1 = Project4_1.Wallet.new()
    wallet2 = Project4_1.Wallet.new()

    Project4_1.Transaction.new(
      wallet1[:address],
      [],
      50,
      wallet2[:address],
      20,
      {wallet1[:private_key], wallet1[:public_key]}
    )
  end

  test "CHECK WALLET CREATION" do
    [{pid, state}] = start_genservers(1)
    wallet = state[:wallet]
    public_key = Project4_1.Wallet.get_public_key(wallet)
    address = Project4_1.Wallet.get_wallet_address(wallet)
    balance = Project4_1.Wallet.get_wallet_balance(wallet)
    signature = :crypto.sign(:ecdsa, :sha256, "message", [wallet[:private_key], :secp256k1])
    assert address === :crypto.hash(:ripemd160, :crypto.hash(:sha256, public_key))
    assert balance === 0.0

    assert :crypto.verify(:ecdsa, :sha256, "message", signature, [public_key, :secp256k1]) ===
             true

    kill_genservers([{pid, state}])
  end

  test "GENESIS BLOCK MINING" do
    list_pids = start_genservers(10)

    pids =
      for {pid, _} <- list_pids do
        pid
      end

    for pid <- pids do
      Project4_1.BitcoinNode.set_list_pids(pid, pids)
    end

    genesis_pid =
      receive do
        {:genesis_block, pid} ->
          # IO.puts("Genesis Block: #{Kernel.inspect(pid)}")
          pid
      end

    genesis_state = GenServer.call(genesis_pid, :get_state)

    assert Project4_1.Wallet.get_wallet_balance(genesis_state[:wallet]) == 50.0

    kill_genservers(list_pids)
  end

  test "SEND MONEY" do
    num_nodes = 10
    list_pids = start_genservers(num_nodes)

    pids =
      for {pid, _} <- list_pids do
        pid
      end

    for pid <- pids do
      Project4_1.BitcoinNode.set_list_pids(pid, pids)
    end

    genesis_pid =
      receive do
        {:genesis_block, pid} ->
          # IO.puts("Genesis Block: #{Kernel.inspect(pid)}")
          pid
      end

    # genesis_state = GenServer.call(genesis_pid, :get_state)
    pay_pid = rand_pid(genesis_pid, pids, num_nodes)

    Project4_1.BitcoinNode.send_money(
      genesis_pid,
      GenServer.call(pay_pid, :get_wallet_address),
      5
    )

    pay_pid_state = GenServer.call(pay_pid, :get_state)
    genesis_state = GenServer.call(genesis_pid, :get_state)

    Process.sleep(1000)

    assert pay_pid_state[:wallet][:balance] === 5.0
    assert genesis_state[:wallet][:balance] === 45.0
    kill_genservers(list_pids)
  end

  test "SECOND BLOCK MINED" do
    num_nodes = 10
    list_pids = start_genservers(num_nodes)

    pids =
      for {pid, _} <- list_pids do
        pid
      end

    for pid <- pids do
      Project4_1.BitcoinNode.set_list_pids(pid, pids)
    end

    genesis_pid =
      receive do
        {:genesis_block, pid} ->
          # IO.puts("Genesis Block: #{Kernel.inspect(pid)}")
          pid
      end

    for _i <- 1..4 do
      pay_pid = rand_pid(genesis_pid, pids, num_nodes)
      # IO.puts("#{Kernel.inspect(genesis_pid)}: Sending $5 to #{Kernel.inspect(pay_pid)}")

      Project4_1.BitcoinNode.send_money(
        genesis_pid,
        GenServer.call(pay_pid, :get_wallet_address),
        5
      )
    end

    Process.sleep(3000)

    for pid <- pids do
      state = GenServer.call(pid, :get_state)
      assert Enum.count(state[:block_map]) + Enum.count(state[:last_block_map]) > 1
    end

    kill_genservers(list_pids)
  end

  test "CHECKING NONCE" do
    wallet1 = Project4_1.Wallet.new()
    wallet2 = Project4_1.Wallet.new()

    transaction =
      Project4_1.Transaction.new(
        wallet1[:address],
        [],
        50,
        wallet2[:address],
        20,
        {wallet1[:private_key], wallet1[:public_key]}
      )

    transaction_map = %{Project4_1.Utils.hash_transaction(transaction) => transaction}
    map_length = Enum.count(transaction_map)
    merkle_tree_root = Project4_1.BitcoinNode.get_merkle_tree_root(transaction_map, map_length)
    block_header = Project4_1.BlockHeader.new(nil, merkle_tree_root, 4, 0)
    block = Project4_1.Block.new(block_header, transaction_map)
    {_status, mined_block} = Project4_1.BitcoinNode.mine_block(block)
    <<init_bits::4, _rest_bits::252>> = Project4_1.Utils.hash_block_header(mined_block)
    assert init_bits === 0
  end

  test "MERKLE ROOT HASH" do
    n = 3

    transaction_map =
      Enum.reduce(1..n, %{}, fn _i, transaction_map ->
        transaction = create_transaction()
        Map.put(transaction_map, Project4_1.Utils.hash_transaction(transaction), transaction)
      end)

    root_hash =
      Project4_1.BitcoinNode.get_merkle_tree_root(transaction_map, Enum.count(transaction_map))

    {_, transaction0} = Enum.at(transaction_map, 0)
    {_, transaction1} = Enum.at(transaction_map, 1)
    {_, transaction2} = Enum.at(transaction_map, 2)

    hash0 = Project4_1.Keys.get_hash(Kernel.inspect(transaction0) <> Kernel.inspect(transaction1))
    hash1 = Project4_1.Keys.get_hash(Kernel.inspect(transaction2) <> Kernel.inspect(transaction2))
    hash2 = Project4_1.Keys.get_hash(Kernel.inspect(hash0) <> Kernel.inspect(hash1))
    assert hash2 === root_hash
  end

  test "TRANSACTION VERIFICATION WITH NO INPUTS" do
    wallet1 = Project4_1.Wallet.new()
    wallet2 = Project4_1.Wallet.new()

    transaction =
      Project4_1.Transaction.new(
        wallet1[:address],
        [],
        50,
        wallet2[:address],
        20,
        {wallet1[:private_key], wallet1[:public_key]}
      )

    assert Project4_1.Transaction.legitimate?(transaction, %{}) === true
  end

  test "TRANSACTION VERIFICATION WITH INPUTS" do
    num_nodes = 10
    list_pids = start_genservers(num_nodes)

    pids =
      for {pid, _} <- list_pids do
        pid
      end

    for pid <- pids do
      Project4_1.BitcoinNode.set_list_pids(pid, pids)
    end

    genesis_pid =
      receive do
        {:genesis_block, pid} ->
          # IO.puts("Genesis Block: #{Kernel.inspect(pid)}")
          pid
      end

    pay_pid = rand_pid(genesis_pid, pids, num_nodes)

    genesis_state = GenServer.call(genesis_pid, :get_state)
    pay_state = GenServer.call(pay_pid, :get_state)
    utxos = genesis_state[:txn_utxos]

    {_, transaction} =
      Project4_1.Wallet.create_new_transaction(
        genesis_state[:wallet],
        pay_state[:wallet][:address],
        5
      )

    assert Project4_1.Transaction.legitimate?(transaction, utxos) === true

    kill_genservers(list_pids)
  end

  test "SIGNATURE AND VERIFICATION" do
    wallet = Project4_1.Wallet.new()
    wallet_impostor = Project4_1.Wallet.new()
    message = "message"
    signature = Project4_1.Keys.sign(message, wallet[:private_key])
    assert Project4_1.Keys.verify(message, signature, wallet[:public_key]) === true
    assert Project4_1.Keys.verify(message, signature, wallet_impostor[:public_key]) === false
  end
end
