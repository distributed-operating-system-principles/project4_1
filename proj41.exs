num_nodes = 10

list_pids =
  for _i <- 1..num_nodes do
    {:ok, pid} = Project4_1.BitcoinNode.start_link(self())
    pid
  end

for pid <- list_pids do
  Project4_1.BitcoinNode.set_list_pids(pid, list_pids)
end

genesis_pid =
  receive do
    {:genesis_block, pid} ->
      IO.puts("Genesis Block: #{Kernel.inspect(pid)}")
      pid
  end

for i <- 1..3 do
  pay_pid = Project4_1.Utils.rand_pid(genesis_pid, list_pids, num_nodes)
  IO.puts("#{Kernel.inspect(genesis_pid)}: Sending $5 to #{Kernel.inspect(pay_pid)}")
  Project4_1.BitcoinNode.send_money(genesis_pid, GenServer.call(pay_pid, :get_wallet_address), 5)
end

receive do
  {:message_type, _value} ->
    nil
    # code
end
